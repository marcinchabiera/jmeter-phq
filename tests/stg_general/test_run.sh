#!/usr/bin/env bash
set -ex

cd tests/stg_general
jmeter -t test_plan.jmx \
       -Jusers=10 -Jramp_up=1 -Jloop_count=1 \
       -Jjmeter.save.saveservice.output_format=xml \
       -n -l test_result.xml
