#!/usr/bin/env bash
set -x

echo
echo "=================================================================================="
JMETER_THREADS=${1}
JMETER_RAMP_UP=${2}
JMETER_LOOP_COUNT=${3}
# JMETER_TRANSACTION_CONTROLLER=${4}

echo
echo "=================================================================================="
if [[ -z $1 && -z $2 && -z $3 && -z $4 ]]; then
    echo "Not all parameters provided"
    exit 1
fi

echo
echo "=================================================================================="
echo "Print info about last commit"
git describe
git log --oneline --decorate -1

echo
echo "=================================================================================="
echo "Check working directory"
pwd
ls -al

echo
echo "=================================================================================="
echo "Add jmeter to path"
working_dir=$(pwd)
export JMETER_HOME=${working_dir}/jmeter/apache-jmeter-3.3
export PATH=$JMETER_HOME/bin:$PATH

echo
echo "=================================================================================="
echo "Install jmeter if needed"
ls jmeter
if [ $? == 0 ]; then
    echo '[=== JMETER ALREADY INSTALLED ===]'
    jmeter -v
else
    mkdir jmeter \
        && cd jmeter/ \
        && pwd \
        && wget https://archive.apache.org/dist/jmeter/binaries/apache-jmeter-3.3.tgz \
        && tar -xzf apache-jmeter-3.3.tgz \
        && rm apache-jmeter-3.3.tgz \
        && cd .. \
        && mkdir jmeter-plugins \
        && cd jmeter-plugins/ \
        && wget https://jmeter-plugins.org/downloads/file/JMeterPlugins-ExtrasLibs-1.4.0.zip \
        && unzip -o JMeterPlugins-ExtrasLibs-1.4.0.zip -d ../jmeter/apache-jmeter-3.3/ \
        && cd ${working_dir}
    echo '[=== JMETER INSTALLED ===]'
    jmeter -v
fi

echo
echo "=================================================================================="
echo "Check working directory"
pwd
ls -al

#echo
#echo "=================================================================================="
#echo "Disable all TRANSACTION_CONTROLLERs in test plan"
#sed -i -e "s/TRANSACTION_CONTROLLER\" enabled=\"true\"/TRANSACTION_CONTROLLER\" enabled=\"false\"/g" tests/${JMETER_TEST_PLAN}/test_plan.jmx
#
#echo
#echo "=================================================================================="
#echo "Enable only selected TRANSACTION_CONTROLLER"
#sed -i -e "s/\"${JMETER_TRANSACTION_CONTROLLER}_TRANSACTION_CONTROLLER\" enabled=\"false\"/\"${JMETER_TRANSACTION_CONTROLLER}_TRANSACTION_CONTROLLER\" enabled=\"true\"/g" tests/${JMETER_TEST_PLAN}/test_plan.jmx

#echo
#echo "=================================================================================="
#echo "List TRANSACTION_CONTROLLERs"
#cat tests/${JMETER_TEST_PLAN}/test_plan.jmx | grep TRANSACTION_CONTROLLER | sed 's/^[[:blank:]]*//'

echo
echo "=================================================================================="
echo "Run jmeter test"
cd tests/${JMETER_TEST_PLAN}
rm test_result.csv
pwd
ls -al

# Output in xml format without Dashboard generator
# jmeter -t test_plan.jmx -Jjmeter.save.saveservice.output_format=xml -n -l test_result.xml -Jusers=${JMETER_THREADS} -Jramp_up=${JMETER_RAMP_UP} -Jloop_count=${JMETER_LOOP_COUNT}

# Output in csv format with Dashboard generator
jmeter -t test_plan.jmx -Jusers=${JMETER_THREADS} -Jramp_up=${JMETER_RAMP_UP} -Jloop_count=${JMETER_LOOP_COUNT} \
 -Jjmeter.save.saveservice.bytes=true \
 -Jjmeter.save.saveservice.label=true \
 -Jjmeter.save.saveservice.latency=true \
 -Jjmeter.save.saveservice.response_code=true \
 -Jjmeter.save.saveservice.response_message=true \
 -Jjmeter.save.saveservice.successful=true \
 -Jjmeter.save.saveservice.thread_counts=true \
 -Jjmeter.save.saveservice.thread_name=true \
 -Jjmeter.save.saveservice.time=true \
 -Jjmeter.save.saveservice.connect_time=true \
 -Jjmeter.save.saveservice.timestamp_format=ms \
 -Jjmeter.save.saveservice.print_field_names=true \
 -n -l test_result.csv

echo
echo "=================================================================================="
echo "Generate Jmeter Dashboard"
rm -rf JMeterDashboard
jmeter -g test_result.csv -o JMeterDashboard
tar -czf JMeterDashboard__build_$BUILD_NUMBER.tar.gz JMeterDashboard

echo
echo "=================================================================================="
echo "==== END OF PROCESSING ===="
echo "=================================================================================="
echo

