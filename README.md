# **Performance testing for PHQ**
1. prepare a test plan for **Load Test** (concurrent users activity in public_api, flex_api, rest_api, webapp)
1. prepare a test plan for **Spike Test** i.e. to determine the upper limit of all the components of application like a database, hardware, network, etc. so that the application can manage the anticipated load in future.
1. prepare a test plan to **compare performance** of releases
1. prepare a test plan to compare the same business functionalities across availables APIs
1. prepare test plans for specific customers behaviours (common operations) 

#### To be considered
* creating new proofs and testing all methods on newly created elements (NOTE: DB volume will be affected)
* testing all methods on existing accounts/proofs

# **Performance testing in general**
* Performance testing is the general name for tests that check how the system behaves and performs. Performance testing examines responsiveness, stability, scalability, reliability, speed and resource usage of your software and infrastructure. Different types of performance tests provide you with different data, as we will further detail.
* The primary goal of performance testing includes establishing the benchmark behavior of the system.
* Performance testing does not aim to find defects in the application

## **Load testing**
* Checks how systems function under a heavy number of concurrent virtual users performing transactions over a certain period of time. Or in other words, how systems handle heavy load volumes.  
* Load testing is meant to test the system through constantly and steadily increasing the load on the system until it reaches the threshold limit
* The sole purpose of load testing is to assign the system the largest job it can possibly handle to test the endurance of system and monitor the results.

### Other names
* **volume testing** - mainly focuses on databases
* **endurance testing** - tests the system by keeping it under a significant load for a sustained time period

### GOALS
* Exposing the defects in application related to buffer overflow, memory leaks and mismanagement of memory. The issues that would eventually come out as the result of load testing may include load balancing problems, bandwidth issues, the capacity of the existing system, etc.    
* To determines the upper limit of all the components of application like a database, hardware, network, etc. so that the application can manage the anticipated load in future.

## **Stress testing**
* Checks the upper limits of your system by testing it under extreme loads. The testing examines how the system behaves under intense loads, and how it recovers when going back to normal usage.
* Under stress testing, various activities to overload the existing resources with excess jobs are carried out in an attempt to break the system down.
* **Negative testing** which includes removal of the components from the system is also done as a part of stress testing.    
* Also known as **fatigue testing**, this testing should capture the stability of the application by testing it beyond its bandwidth capacity.
* Basically, stress testing evaluates the behavior of the application beyond peak load and normal conditions.

### TYPES
* **Spike Test** - stress test that includes a sudden ramp-up in the number of virtual users
* **Soak Test** - stress test for a long period of time to check the system’s sustainability over time with a slow ramp-up
    
## **KPIs Examples**
* number of virtual users
* hits per second
* errors per second
* response time 
* latency 
* bytes per second (throughput)

# **Jenkins/JMeter - Problem statement**
How to run Apache JMeter tests on test environment as Jenkins build?
 
### Options to be considered
1. JMeter docker container launched by jenkins job + test plan launched by jenkins job within docker container 
2. JMeter installed on jenkins machine and test plan launched by jenkins job
3. **SELECTED**: JMeter installed by jenkins job and test plan launched by jenkins job 

    **Example**: http://jenkins.dev1.int.proofhq.com:8080/job/jmeter-phq-public-api-all/

### JMeter plugin
* Jenkins **Performance Plugin** must be installed and configured on Jenkins to display results of historical JMeter builds .
* https://wiki.jenkins.io/display/JENKINS/Performance+Plugin
